fine = true;
//fine = false;

$fn = fine ? 100 : 25;



base_size = 25.5;
plate_thickness = base_size / 8;



use <lib/cover.scad>;

rotate([180, 0, 0]) cover(2, 1, 0.5);

linear_extrude(height = 1)
translate([base_size, -base_size/2, 0]) {
    spread = 5;

    translate([0, spread, 0]) {
        text("0.5mm shim", size = 6, halign = "center", valign = "center", spacing = .9);
    }
    translate([0, -spread, 0]) {
        text("10\u00B0 tilt", size = 6, halign = "center", valign = "center");
    }
}
