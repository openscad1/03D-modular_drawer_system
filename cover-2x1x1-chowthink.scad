fine = true;
//fine = false;

$fn = fine ? 100 : 25;



base_size = 25.5;
plate_thickness = base_size / 8;



use <lib/cover.scad>;

rotate([180, 0, 0]) cover(2, 1, 0.2);

linear_extrude(height = 1)
translate([base_size, -base_size/2, 0])
    text("chow think", size = 6, halign = "center", valign = "center");
