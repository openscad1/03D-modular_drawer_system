include <peglib/defaults.scad>;
include <moslib/libchamfer.scad>;
include <peglib/rail.scad>;

wideness = 2.5;

module innie() {
    
    intersection() {
        color("orange") {
            translate([-1, -37.8297, 0]) {
                import("lib/Interlocking_Module_V3.stl");
            }
        }

        color("red") {
            translate([-1 * ((base_size * wideness) / 2), 0, -10]) {
                cube([base_size * 2.5, 24, 120]);
            }
        }
    }
} 




module outie() {
    
    intersection() {
        color("orange") {
            to_align_rhs = -0.4;
            to_align_lhs = 1.05;
            translate([(to_align_lhs + to_align_rhs) / 2, -38.5, 0]) {
                rotate([0, 0, 90]) {
                    import("lib/Interlocking_Module_V3.stl");
                }
            }
        }

        color("red") {
            translate([-1 * ((base_size * wideness) / 2), 0, -10]) {
                cube([base_size * 2.5, 24, 120]);
            }
        }
    }
    
} 






module horizontal_rail_2point5() {
    translate([0, -base_size / 2, wall_thickness * 2])
        rotate([90, 0, 0]) {
        for (x = [-1, 1]) {
            difference() {
                color("salmon")
                translate([x * (base_size * (wideness / 2)), 0, 0])
                rotate([0, x * (-90), 0])
                rail(height = 2, chamfer = wall_thickness / 4, back_wall_gap = default_back_wall_gap, side_wall_gap = 0, copies = 1);
                
                color("red") 
                translate([x * ((-base_size / 2) - 1), 0, 0])
                cube([base_size, base_size, base_size * 2], center = true);
            }
        }
    }
}


module wedgie() {
    color("rebeccapurple") {
        hull() {
            color("limegreen") 
            translate([0, -(base_size / 2), wall_thickness * 2])
            chamfered_box(dim = [base_size * wideness, base_size, wall_thickness], align = [0, 0, 1], chamfer = wall_thickness / 4);

            translate([0, -1, wall_thickness * 3]) {
                difference() {
                    color("fuchsia")
                    chamfered_box(dim = [base_size * 2, wall_thickness, 100 - (wall_thickness * 2)], align = [0, 1, 1], chamfer = wall_thickness / 4);
                
                    color("sandybrown")
                    chamfered_box(dim = [base_size * wideness + 1, wall_thickness, 200 - (wall_thickness * 2)], align = [0, 1.5, .95]);
                }
            }

        }
    }
}


difference() {
    union() {
        translate([0, 0, 0.2])
        horizontal_rail_2point5();
        
        translate([0, 0, 1])
        wedgie();

        translate([0, 0, 4.5])
        union() {
            translate([0, 0, -0.17])
            innie();

/*
            color("slategrey")
            chamfered_box(dim = [base_size * wideness, wall_thickness * (2/3), 4.4], align = [0, 0, 1]);
*/
        }
    }

/*    color("tomato")
    translate([0, 0, 75])
    cube([(base_size * wideness) + 1, base_size, 100], center = true);
    */

}