// reference block size
//
height = 74.6;
width = 74.6;
length = 95;




base_size = 25.4;




module reference_module() {
    translate([-1, -0.25, 0]) {
        import("lib/Interlocking_Module_V3.stl");
    } 
}

module reference_block() {
    color("orange") {
         cube([width, height, length], center = true);
    }
    
}

module reference_drawer() {
    color("salmon") {
        import("lib/Drawer.stl");
    }
}


reference_module();
reference_block();
reference_drawer();