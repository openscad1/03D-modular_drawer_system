
height = 74.6;
width = 74.6;
length = 95;

hole_diameter = 25.4 /4;
hole_spacing = 25.4;

$fn = 100;

module drawer() {
    
    // reference block
    //
    color("orange") {
        //cube([width, length, height], center = true);
    }
    
    
    // drawer
    //
    difference() {
        union() {
            // draw two to cover the knob mounting holes
            //
            translate([0,-95/2,-74.6/2]) {
                color("royalblue") {
                    import("lib/Double_Width_Drawer.stl");
                }
            }
            translate([0,95/2,-74.6/2]) {
                rotate([0, 0, 180]) {    
                    color("skyblue") {
                        import("lib/Double_Width_Drawer.stl");
                    }
                }
            }
        }
        
        // finger loop
        //
        color("green") {
            translate([0, -45, 32.4172 + 15]) {
                rotate([90, 0, 0]) {
                    cylinder(d = 60, h = 10, center = true);
                }
            }
        }
    }
}


module pips() {
    translate([0, -length / 2, 0]) {
        rotate([90, 0, 0]) {
            union() {
                translate([-hole_spacing / 2, 0, 0]) cylinder(d = hole_diameter, h = 10, center = true);
                translate([ hole_spacing / 2, 0, 0]) cylinder(d = hole_diameter, h = 10, center = true);
            }
        }
    }
}


scale_factor = 0.995;
scale([scale_factor, scale_factor, scale_factor]) {

   difference() {
        drawer();
        color("red") pips();
        
   }



}