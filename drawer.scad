
height = 74.6;
width = 74.6;
length = 95;

hole_diameter = 25.4 /4;
hole_spacing = 25.4;

$fn = 100;

module drawer() {

    // reference block
    color("orange") {
        // cube([width, length, height], center = true);
    }
    
    
    rotate([0, 0, 180]) {
        translate([74.6, -172.5 , 74.6]) {
            color("blue") {
                import("lib/Drawer.stl");
            }
        }
    }
}


module pips() {
    translate([0, -length / 2, 0]) {
        rotate([90, 0, 0]) {
            union() {
                translate([-hole_spacing / 2, 0, 0]) cylinder(d = hole_diameter, h = 10, center = true);
                translate([ hole_spacing / 2, 0, 0]) cylinder(d = hole_diameter, h = 10, center = true);
            }
        }
    }
}

scale_factor = 0.995;
scale([scale_factor, scale_factor, scale_factor]) {


    difference() {
        color("royalblue") {
            drawer();
        }
        color("red") {
            pips();
        }
    }


}
