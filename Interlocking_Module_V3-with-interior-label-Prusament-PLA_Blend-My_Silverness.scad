// reference block size
//
height = 74.6;
width = 74.6;
length = 95;


fudge = -0.5; 

base_size = 25.4;

// font_size was 6 until "Lime Green (Blend)", March 2021
font_size = 5; 


difference() {

    translate([-1, -0.25, 0]) {
        import("lib/Interlocking_Module_V3.stl");
    } 


    text_brand = "Prusament PLA Blend";
    text_color = "My Silverness";
    
    base_size = 25.4;
    text_height = 1;
    
    color("red") {
        translate([0, 0.02875 + (fudge + (width + text_height) / 2), 0]) {
            translate([0, text_height / 2, length - 10]) {
                rotate([90, 180, 0]) {
                    linear_extrude(height = 1) {
                            spread = 5;
                            translate([0, spread, 0]) {
                                text(text_brand, size = font_size, halign = "center", valign = "center", spacing = .9);
                            }
                            translate([0, -spread, 0]) {
                                text(text_color, size = font_size, halign = "center", valign = "center");
                            }
                        }
 
                }
            }
        }
    }
    
    
        
    color("blue") {
        translate([0, ((-0.7 -(width + text_height) / 2) - fudge) - 0.15, 0]) {
            translate([0, text_height / 2, length - 10]) {
                rotate([90, 180, 180]) {
                    linear_extrude(height = 1) {
                            spread = 5;
                            translate([0, spread, 0]) {
                                text(text_brand, size = font_size, halign = "center", valign = "center", spacing = .9);
                            }
                            translate([0, -spread, 0]) {
                                text(text_color, size = font_size, halign = "center", valign = "center");
                            }
                    }
                }
            }
        }
    }
    
    
    color("orange") {
        translate([(fudge + (0.2 + (width + text_height) / 2)) + 0.24, 0]) {
            translate([0, text_height / 2, length - 10]) {
                rotate([90, 180, 270]) {
                    linear_extrude(height = 1) {
                            spread = 5;
                            translate([0, spread, 0]) {
                                text(text_brand, size = font_size, halign = "center", valign = "center", spacing = .9);
                            }
                            translate([0, -spread, 0]) {
                                text(text_color, size = font_size, halign = "center", valign = "center");
                            }
                    }
                }
            }
        }
    }
    
    color("grey") {
        translate([((-0.2 -(width + text_height) / 2) - fudge) -0.23, 0, 0]) {
            translate([0, text_height / 2, length - 10]) {
                rotate([90, 180, 90]) {
                    linear_extrude(height = 1) {
                            spread = 5;
                            translate([0, spread, 0]) {
                                text(text_brand, size = font_size, halign = "center", valign = "center", spacing = .9);
                            }
                            translate([0, -spread, 0]) {
                                text(text_color, size = font_size, halign = "center", valign = "center");
                            }
                    }
                }
            }
        }
    }
    
}



module reference_block() {
    color("orange") {
         cube([width, height, length], center = true);
    }
    
}
