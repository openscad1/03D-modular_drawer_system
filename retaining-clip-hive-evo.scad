channel_width = 15;
channel_length = 80; 

rail_head_width = channel_width;
rail_base_width = 4.5;

headroom = 0.7;

foo = 3;

slice = rail_base_width / 5;

shrinkage = 0.875;

module 5x5_hook() { 
    headroom = 1;
    translate([-1.25, -1.25, - headroom / 2]) {
        scale([.25,.25,1]) {
            color("midnightblue") {
                union() {
                 
                    translate([0, 0, headroom / 2]) {
                        cube([10, 10, headroom], center = true);
                        translate([5, 10, 0]) {
                            cube([20, 10, headroom], center = true);
                        }
                    }
                    

                    translate([5, -5, 0]) {
                        scale([1, 1, 1]) {
                            hull() {

                                Points = [
                                    [  0,  0,  0 ],  //0
                                    [ 10, 10,  0 ],  //1
                                    [  0, 10,  0 ],  //2

                                    [  0,  0,  headroom ],  //3
                                    [ 10, 10,  headroom ],  //4
                                    [  0, 10,  headroom ]  //5
                                ];

                                Faces = [
                                    [0,1,2],    // bottom
                                    [3,4,5]    // top
                                ]; 

                                polyhedron(Points, Faces);
                                
                            }
                        }
                    }
                }
            }
        }
    }
}


difference() {

    union() {

        // spine
        //
        color("orange") {
            translate([0, 0, headroom / 2 ]) {
                cube([rail_head_width, channel_length, headroom], center = true);
            }
        }


        // horizontal hooks
        //
        for(y = [-1, 1]) {
            for(x = [-1, 1]) {
                translate([x * (((rail_head_width - 5) / 2) + (2.5 * 0.875)), y * ((channel_length / 2) + (2.5 * shrinkage)), headroom / 2 ]) {
                    rotate([y > 0 ? 180 : 0, x < 0 ? 180 : 0, 0]) {
                        color("red")  scale([shrinkage,shrinkage,headroom]) 5x5_hook();
                    }
                }
            }
        }


        // vertical hooks
        //
        hooky = 1.5;
        for(y = [-1, 1]) {
            for(x = [-1, 1]) {
                difference() {
                    union() {
                        translate([x * (((rail_head_width - 5) / 2) + 1), y * ((channel_length / 2) + (2.5 * shrinkage)) - (y * hooky), 2.51 * shrinkage]) {
                            rotate([y > 0 ? 180 : 0, y < 0 ? 180 : 0, 0]) {
                                rotate([0, 90, 0]) {
                                    color("green")  scale([shrinkage,shrinkage,2]) 5x5_hook();
                                }
                            }
                        }
                        translate([x * (((rail_head_width - 5) / 2) + 1), y * ((channel_length / 2) + (2.5 * shrinkage)) - (y * 2.5) - (y * hooky), 2.51 * shrinkage]) {
                            rotate([y > 0 ? 180 : 0, y < 0 ? 180 : 0, 180]) {
                                rotate([0, 90, 0]) {
                                    color("yellow")  scale([shrinkage,shrinkage,2]) 5x5_hook();
                                }
                            }
                        }
                    }
                    color("red") {
                        translate([x * 11, y * 36, 0]) {
                            rotate([0, 0, x * y * -30]) {
                                cube([10, 10, 10], center = true);
                            }
                        }
                    }
                }


            }
        }

    }

    
        // spine
        //
        color("pink") {
            for(y = [-1, 1]) {
                hull() {
                    translate([0, y * (channel_length / 2), headroom / 2 ]) {
                        cube([rail_head_width - 5, 1, 10], center = true);
                    } 
                    translate([0, y * (channel_length / 4), headroom / 2 ]) {
                        cube([0.01, 1, 10], center = true);
                    } 
                }
            }
        }



    cubishness = 0.01;
    for(x = [-1, 1]) {

        color("fuchsia") hull() {
            translate([x * ((rail_head_width / 2) + (cubishness / 2)) - (x * 3), 0, 0]) {
                color("fuchsia") cube([cubishness, cubishness, 10], center = true);
            }
            translate([x * ((rail_head_width / 2) + (cubishness / 2)), (channel_length / 2) - (cubishness / 2), 0]) {
                color("fuchsia") cube([cubishness, cubishness, 10], center = true);
            }
            translate([x * ((rail_head_width / 2) + (cubishness / 2)), -1 * ((channel_length / 2) - (cubishness / 2)), 0]) {
                color("fuchsia") cube([cubishness, cubishness, 10], center = true);
            }
        }
    }
    
}