
height = 74.6;
width = 74.6;
length = 95;

hole_diameter = 25.4 /4;
hole_spacing = 25.4;

$fn = 100;

module box() {
    
    // reference block
    //
    color("orange") {
        //cube([width, length, height], center = true);
    }
    
    
    // module
    //
    translate([-1, 50, -0.375]) {
        color("royalblue") {
            rotate([90, 0, 0]) {
                import("lib/Interlocking_Module_V3.stl");
            }
        }
    }
   
}


module bump() {
    bumpiness = 4;
    translate([0, (-length / 2) + 2, 38]) {
        difference() {
            rotate([45, 0, 0]) {
                cube([75, bumpiness, bumpiness], center = true);
            }
            translate([0, 0, bumpiness / 2]) {
                cube([80, bumpiness * 2, bumpiness], center = true);
            }
        }
    }
}


// scaling drawer down 1/2 a percent seems to work well
//
//scale_factor = 0.995;


// don't scale the modules
//
scale_factor = 1.000;

scale([scale_factor, scale_factor, scale_factor]) {

    box();
    color("red") bump();

}