
height = 74.6;
width = 74.6;
length = 95;

hole_diameter = 25.4 /4;
hole_spacing = 25.4;

$fn = 100;

module drawer() {
    // reference block
    color("orange") {
    //    cube([width, length, height], center = true);
    }
    translate([-10, 4.5, -37.3]) {
        import("lib/drawer_with_finger_loop.stl");
    }
}

module pips() {
    translate([0, -length / 2, 0]) {
        rotate([90, 0, 0]) {
            union() {
                translate([-hole_spacing / 2, 0, 0]) cylinder(d = hole_diameter, h = 10, center = true);
                translate([ hole_spacing / 2, 0, 0]) cylinder(d = hole_diameter, h = 10, center = true);
            }
        }
    }
}


intersection() {
    difference() {
        color("royalblue") drawer();
        color("red") pips();
    }

    color("cyan") translate([0, -length / 2, 0]) cube([40, 10, 40], center = true);
}
